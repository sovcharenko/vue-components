Vue.component('tab-menu', {
    template: `
        <div v-bind:id="containerID" v-bind:class="containerClass">
            <a v-for="link in tabLinks"
                    @click.prevent="setTab(link.tab)" 
                    v-bind:class="getLinkClass(link.tab)">
                    {{ link.label }}
            </a>  
        </div>
    `,

    props: {
        currentLayoutTab: {
            default: '.index'
        },
        layoutTabClass:{
            default: '.layout-tab'
        },
        layoutTabToggleClass: {
            default: 'hidden'
        },
        containerID: String,
        containerClass: String,
        linkClass: {
            default: 'tab-link'
        },
        linkToggleClass: {
            default: 'active'
        }
    },

    data: function() {
        return {
            tabLinkClass: this.linkClass,
            currentLayoutTabClass: this.currentLayoutTab,
            tabLinks: [
                {
                    label: 'Home',
                    tab: this.currentLayoutTab,
                    active: true
                },
                {
                    label: 'Extended Select',
                    tab: '.extSelectContainer'
                },
                {
                    label: 'Simple Form builder',
                    tab: '.formBuilderContainer'
                }
            ]
        };
    },
    
    created: function () {
        var layoutTabs = document.querySelectorAll(this.layoutTabClass);
        for(var i = 0; i < layoutTabs.length; i++ ) {
            layoutTabs[i].classList.add(this.layoutTabToggleClass);
        }
        document.querySelector(this.layoutTabClass + this.currentLayoutTab).classList.remove(this.layoutTabToggleClass);
    },

    methods: {
        getLinkClass: function (tab) {
            return this.linkClass +
                (this.currentLayoutTabClass == tab ? (' ' + this.linkToggleClass) : '');
        },

        setTab: function (tab) {
            var layoutTabs = document.querySelectorAll(this.layoutTabClass);
            for(var i = 0; i < layoutTabs.length; i++ ) {
                layoutTabs[i].classList.add(this.layoutTabToggleClass);
            }
            document.querySelector(this.layoutTabClass + tab).classList.remove(this.layoutTabToggleClass);
            this.currentLayoutTabClass = tab;
        }
    }
});

new Vue({ el: '.layout-menu' });