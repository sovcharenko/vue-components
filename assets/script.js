Vue.component('extSelect', {

    template: `
        <span class="ext-select-container">
            <span class="ext-select" v-bind:class="{ 'expanded': containerExpanded }">
               <span class="ext-select-input-container">
                    <input class="ext-select-input" type="text" 
                        v-bind:placeholder="elPlaceholder" 
                        v-bind:value="current.label" 
                        v-model="currentInput" 
                        
                        @focus="expand"
                        @blur="close"
                        
                        @keyup="filter"
                        @keyup.down="move('down')"
                        @keyup.up="move('up')"
                        @keyup.enter="checkUpdateModel()">
                    <span v-if="current.id" class="ext-select-clear" @click="clear">&times;</span>
                    <span class="ext-select-arrow" @click="toggle">&#x25BE;</span>
               </span>
               <input type="hidden" v-bind:id="elId" v-bind:name="elName" v-bind:value="current.id">
               <ul class="el-container" v-bind:class="{ 'el-container-expanded': containerExpanded }">
                  <li v-for="el in filtered"
                    v-bind:data-value="el.id"
                    v-bind:class="{ 'el': true, 'el-selected':(el.id == current.id), 'el-not-found':(el.notFound) }"
                    
                    @mouseover="updateModel(el)"
                    @click="close">
                    {{el.label}}
                  </li>
               </ul>
            </span>
        </span>
    `,

    props: [
        'elId',
        'elName',
        'elValue',
        'elPlaceholder',
        'elData',
        'allowTags'
    ],

    data: function() {
        return {
            filtered: this.elData,
            currentInput: '',
            current: {
                id: (this.elValue ? this.elValue : ''),
                label: ''
            },

            containerExpanded: false
        };
    },

    methods: {
        filter: function() {
            if (['ArrowUp','ArrowDown','Enter'].indexOf(event.code) == -1) {
                if (!this.containerExpanded) this.expand();
                this.current = {};
                var filteredData = [];
                for(var i = 0; i < this.elData.length; i++) {
                    if ( this.elData[i].label.toLowerCase().indexOf(this.currentInput.toLowerCase()) != -1) {
                        filteredData.push(this.elData[i]);
                    }
                }
                if (filteredData.length < 1) {
                    filteredData.push({
                        id:'',
                        label: '"' + this.currentInput + '" not found',
                        notFound: true
                    });
                }
                this.filtered = filteredData;

            }
        },
        move: function (where) {
            for(var i = 0; i < this.filtered.length; i++) {
                if ( this.filtered[i].id == this.current.id) {
                    var elNum = (where == 'down' ? (i + 1) : (i - 1));
                    if (typeof this.filtered[elNum] === 'undefined') return;
                    this.updateModel( this.filtered[elNum] );
                    return;
                }
            }
            this.updateModel( this.filtered[0] );
        },

        clear: function () {
            this.filtered = this.elData;
            this.current = {};
            this.currentInput = '';
        },

        checkUpdateModel: function () {
            for(var i = 0; i < this.filtered.length; i++) {
                if ( this.filtered[i].id == this.current.id ) {
                    this.close();
                    return;
                }
            }
            if (this.allowTags) {
                this.current = {
                    id: this.currentInput,
                    label: this.currentInput
                };
            } else {
                this.updateInput('');
            }
            this.close();
        },

        updateModel: function (obj) {
            if (obj.hasOwnProperty('notFound')) return;
            this.current = obj;
            this.updateInput(this.current.label);
        },
        updateInput: function (val) {
            this.currentInput = val;
        },

        expand: function () {
            this.containerExpanded = true;
        },
        close: function () {
            this.containerExpanded = false;
        },
        toggle: function () {
            this.containerExpanded = !this.containerExpanded;
        }
    }
});
var vmContainer = new Vue({
    el: '#extSelectContainer',
    data: {
        extSelectElements: {
            name: 'extselectName',
            id: 'extselectID',
            placeholder: 'Pls select smth...',
            data: [
                {
                    id: '1',
                    label: 'Nathan Drake'
                },
                {
                    id: '2',
                    label: 'Ezio A. DaFirenzce'
                },
                {
                    id: '3',
                    label: 'Jabba Hadt'
                },
                {
                    id: '4',
                    label: 'Altair Ibn La Akhaad'
                },
                {
                    id: '5',
                    label: 'Aragorn s.of Arathorn'
                },
                {
                    id: '6',
                    label: 'Shepard Cap.of Normandy'
                },
                {
                    id: '7',
                    label: 'Liara T\'sony'
                },
                {
                    id: '8',
                    label: 'Bruce Wayne'
                },
                {
                    id: '9',
                    label: 'Stephen Scorceze'
                },
                {
                    id: '10',
                    label: 'Dick Grason'
                },
                {
                    id: '11',
                    label: 'Arthur Gordon'
                },
                {
                    id: '12',
                    label: 'Eduard Nigma'
                },
                {
                    id: '13',
                    label: 'James May'
                },
                {
                    id: '14',
                    label: 'Richard Hammond'
                },
                {
                    id: '15',
                    label: 'Jeremy Clarckson'
                },
                {
                    id: '16',
                    label: 'Eduard Kenway'
                },
                {
                    id: '17',
                    label: 'Arno Victor Dorian'
                },
                {
                    id: '18',
                    label: 'Shay Patrick Kormack'
                },
                {
                    id: '19',
                    label: 'Jacob Fry'
                },
                {
                    id: '20',
                    label: 'Lilly Fry'
                },
                {
                    id: '21',
                    label: 'Nicola Tesla'
                },
                {
                    id: '22',
                    label: 'Nicollo Mackiavelly'
                },
                {
                    id: '23',
                    label: 'Rowen Atkinson'
                }
            ]
        }
    }
});
Vue.component('formBuilder', {

    template: ` 
        <div class="builder">
            
            <!-- Form -->
            <div class="form-container">
                <form :action="formAction">
                    <h1>{{ formName }}</h1>
                    <div v-for="field in fields" class="form-option">
                        
                        <div>
                            <label :for="field.id">{{ field.label }}</label>
                        </div>
                        
                        <div v-if="field.type.id == 'text'">
                            <input type="text" :id="field.id" :name="field.name" :placeholder="field.placeholder" :value="field.value">
                        </div>
                        
                        <div v-if="field.type.id == 'checkbox'">
                            <input type="checkbox" :id="field.id" :name="field.name" :placeholder="field.placeholder" :value="field.value">                        
                        </div>
                        
                        <div v-if="field.type.id == 'checkboxList'">
                            <div v-for="(option, index) in field.options">
                                <input type="checkbox" 
                                    :id="field.id + '-' + index" 
                                    :name="field.name + '[' + index + ']'" 
                                    :value="option.value"> {{ option.label }}
                            </div>
                        </div>
                        
                        <div v-if="field.type.id == 'radio'">
                            <div v-for="(option, index) in field.options">
                                <input type="radio" 
                                    :id="field.id + '-' + index" 
                                    :name="field.name" 
                                    :value="option.value"> {{ option.label }}
                            </div>
                        </div>
                        
                        <div v-if="field.type.id == 'select'">
                            <select :name="field.name" :id="field.id">
                                <option v-for="(option, index) in field.options" 
                                    :value="option.value">
                                    {{ option.label }}
                                </option>
                            </select>
                        </div>
                        
                    </div>
                </form>
            </div>
            
            <!-- Form Editor -->
            <div class="editor-container">
                <!-- Buttons  -->
                <a class="form-button" @click.prevent="openModal('formOptions')"><b>&#x2264;</b>  Form Options</a>
                <a class="form-button" @click.prevent="openModal('newField')"><b>+</b>  New Field</a>
                
                <!-- Form Options  -->
                <div class="editor-modal" :class="{'open': modal.formOptions}">
                    <div class="editor-modal-container">
                        <div class="editor-modal-header">
                            <h3>Form Options</h3>
                            <a class="editor-modal-close" @click.prevent="closeModal('formOptions')" href="#">&times;</a>
                        </div>
                        <div class="editor-modal-content">
                            <div class="form-options-container">
                                <div class="form-option">
                                    <div>
                                        <label for="formAction">Form Action</label>                
                                    </div>
                                    <div>
                                        <input v-model="formAction" id="formAction" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="formName">Form Name</label>            
                                    </div>
                                    <div>
                                        <input v-model="formName" id="formName" type="text">   
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for=""></label>
                                    </div>
                                    <div>
                                                    
                                    </div>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- New Field  -->
                <div class="editor-modal" :class="{'open': modal.newField}">
                    <div class="editor-modal-container">
                        <div class="editor-modal-header">
                            <h3>New Field</h3>
                            <a class="editor-modal-close" @click.prevent="closeModal('newField')" href="#">&times;</a>
                        </div>
                        <div class="editor-modal-content">
                            <div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldId">ID</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.id" id="newFieldId" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldName">Name</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.name" id="newFieldName" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldPlaceholder">Placeholder</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.placeholder" id="newFieldPlaceholder" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldLabel">Label</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.label" id="newFieldLabel" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldValue">Value</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.value" id="newFieldValue" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldType">Type:<span> {{ newField.type.label }}</span></label>                
                                    </div>
                                    <div>
                                        <select v-model="newField.type" id="newFieldType" type="text">
                                            <option v-for="type in fieldType" :value="type" >{{ type.label }}</option>
                                        </select>            
                                    </div>                    
                                </div>
                                <div v-if="newField.type.isOptional" class="form-option">
                                    <table border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="10%">#</td>
                                                <td width="40%">Label</td>
                                                <td width="40%">Value</td>
                                                <td width="10%">
                                                    <a class="form-button btn-i"
                                                            @click.prevent="newField.options.push(fieldOptionTPL)">
                                                            <b>+</b>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr v-for="(option, index) in newField.options">
                                                <td>{{ index + 1 }}</td>
                                                <td>
                                                    <input type="text">
                                                </td>
                                                <td>
                                                    <input type="text">
                                                </td>
                                                <td>
                                                    <a class="form-button btn-i"
                                                        v-if="(index + 1) > newField.type.minOptions"
                                                        @click.prevent="newField.options.splice(index, 1)">
                                                        &times;
                                                    </a>
                                                </td>
                                            </tr> 
                                        </tbody>
                                       
                                    </table>                    
                                </div>
                            </div>
                        </div>
                        <div class="editor-modal-footer text-right">
                            <a class="form-button"
                                @click="addField">
                                &#x2714;  Add field
                            </a>
                        </div>
                    </div>
                </div>
                
                <div>
                </div>
                
            </div>
        <div>
            
    `,

    props: [

    ],

    data: function() {
        return {

            // -- Form Options
            formAction: '/',
            formName: 'Form',
            formID: this.formName,
            formTitle: 'Form',
            wpapNamesArray: true,

            // -- Modals
            modal: {
                formOptions: false,
                newField: false,
                editField: false
            },

            // -- New Field
            fieldTPL: {
                id: '',
                name: '',
                type: '',
                label: '',
                placeholder: '',
                value: '',
                options: []
            },
            fieldOptionTPL: {
                value: '',
                label: ''
            },
            newField: {},
            currentField: {},
            fieldType: [
                {
                    id: 'text',
                    label: 'Text',
                    isOptional: false
                },
                {
                    id: 'select',
                    label: 'Drop-Down List',
                    isOptional: true,
                    minOptions: 2
                },
                {
                    id: 'checkbox',
                    label: 'Checkbox',
                    isOptional: false
                },
                {
                    id: 'radio',
                    label: 'Radio List',
                    isOptional: true,
                    minOptions: 2
                },
                {
                    id: 'checkboxList',
                    label: 'Checkbox List',
                    isOptional: true,
                    minOptions: 2
                }
            ],

            // -- Fields options
            fields: []
            // wpapNamesArray: true

        };
    },

    created: function() {
        // -- Init
        this.newField = this.fieldTPL;
        this.currentField = this.fieldTPL;

        // -- Default options of the conditional field
        this.$watch('newField.type', function (val, oldVal) {
            if (typeof val != 'undefined' && val.isOptional) {
                var opts = [];
                for(var i = 0; i < val.minOptions; i++)
                    opts.push(this.fieldOptionTPL);
                this.newField.options = opts;
            } else this.newField.options = [];
        })
    },

    methods: {

        // -- Modals
        openModal: function (modalName) {
            for(var m in this.modal) this.modal[m] = false;
            this.modal[modalName] = true;
        },
        closeModal: function (modalName) {
            this.modal[modalName] = false;
        },

        // -- Form fields
        addField: function () {
            console.log(this)
            this.fields.push( JSON.parse( JSON.stringify( this.newField ) ) );
            this.newField = {};
            this.closeModal('newField');
        },

        // -- Field Types
        // getTypeLabel: function (type) {
        //     var typeObject = this.findBy(this.fieldType, 'id', type);
        //     if (typeObject) return ': ' + typeObject.label;
        //     return '';
        // },
        // isOptionalType: function (type) {
        //     return this.findBy(this.fieldType, 'id', type).isOptional;
        // },

        // -- Helpers
        findBy: function (array, key, value) {
            if (Array.isArray(array)) {
                for(var i = 0; i < array.length; i++)
                    if (array[i][key] == value) return array[i];
            } else {
                for (var i in array)
                    if (array[i].hasOwnProperty(key) && (array[i][key] == value)) return array[i];
            }
            return false;
        },
        filterBy: function (array, key, value) {
            if (Array.isArray(array)) {
                return array.filter(function (element) {
                    return (element[key] == value);
                });
            } else {
                var result = {}, element;
                for (element in array) {
                    if (array[element].hasOwnProperty(key) && (array[element][key] == value)) {
                        result[element] = array[element];
                    }
                }
                return result;
            }
        }
    }
});
new Vue({
    el: '#formBuilderContainer',
    data: {

    }
});
Vue.component('tab-menu', {
    template: `
        <div v-bind:id="containerID" v-bind:class="containerClass">
            <a v-for="link in tabLinks"
                    @click.prevent="setTab(link.tab)" 
                    v-bind:class="getLinkClass(link.tab)">
                    {{ link.label }}
            </a>  
        </div>
    `,

    props: {
        currentLayoutTab: {
            default: '.index'
        },
        layoutTabClass:{
            default: '.layout-tab'
        },
        layoutTabToggleClass: {
            default: 'hidden'
        },
        containerID: String,
        containerClass: String,
        linkClass: {
            default: 'tab-link'
        },
        linkToggleClass: {
            default: 'active'
        }
    },

    data: function() {
        return {
            tabLinkClass: this.linkClass,
            currentLayoutTabClass: this.currentLayoutTab,
            tabLinks: [
                {
                    label: 'Home',
                    tab: this.currentLayoutTab,
                    active: true
                },
                {
                    label: 'Extended Select',
                    tab: '.extSelectContainer'
                },
                {
                    label: 'Simple Form builder',
                    tab: '.formBuilderContainer'
                }
            ]
        };
    },
    
    created: function () {
        var layoutTabs = document.querySelectorAll(this.layoutTabClass);
        for(var i = 0; i < layoutTabs.length; i++ ) {
            layoutTabs[i].classList.add(this.layoutTabToggleClass);
        }
        document.querySelector(this.layoutTabClass + this.currentLayoutTab).classList.remove(this.layoutTabToggleClass);
    },

    methods: {
        getLinkClass: function (tab) {
            return this.linkClass +
                (this.currentLayoutTabClass == tab ? (' ' + this.linkToggleClass) : '');
        },

        setTab: function (tab) {
            var layoutTabs = document.querySelectorAll(this.layoutTabClass);
            for(var i = 0; i < layoutTabs.length; i++ ) {
                layoutTabs[i].classList.add(this.layoutTabToggleClass);
            }
            document.querySelector(this.layoutTabClass + tab).classList.remove(this.layoutTabToggleClass);
            this.currentLayoutTabClass = tab;
        }
    }
});

new Vue({ el: '.layout-menu' });
