var vmContainer = new Vue({
    el: '#extSelectContainer',
    data: {
        extSelectElements: {
            name: 'extselectName',
            id: 'extselectID',
            placeholder: 'Pls select smth...',
            data: [
                {
                    id: '1',
                    label: 'Nathan Drake'
                },
                {
                    id: '2',
                    label: 'Ezio A. DaFirenzce'
                },
                {
                    id: '3',
                    label: 'Jabba Hadt'
                },
                {
                    id: '4',
                    label: 'Altair Ibn La Akhaad'
                },
                {
                    id: '5',
                    label: 'Aragorn s.of Arathorn'
                },
                {
                    id: '6',
                    label: 'Shepard Cap.of Normandy'
                },
                {
                    id: '7',
                    label: 'Liara T\'sony'
                },
                {
                    id: '8',
                    label: 'Bruce Wayne'
                },
                {
                    id: '9',
                    label: 'Stephen Scorceze'
                },
                {
                    id: '10',
                    label: 'Dick Grason'
                },
                {
                    id: '11',
                    label: 'Arthur Gordon'
                },
                {
                    id: '12',
                    label: 'Eduard Nigma'
                },
                {
                    id: '13',
                    label: 'James May'
                },
                {
                    id: '14',
                    label: 'Richard Hammond'
                },
                {
                    id: '15',
                    label: 'Jeremy Clarckson'
                },
                {
                    id: '16',
                    label: 'Eduard Kenway'
                },
                {
                    id: '17',
                    label: 'Arno Victor Dorian'
                },
                {
                    id: '18',
                    label: 'Shay Patrick Kormack'
                },
                {
                    id: '19',
                    label: 'Jacob Fry'
                },
                {
                    id: '20',
                    label: 'Lilly Fry'
                },
                {
                    id: '21',
                    label: 'Nicola Tesla'
                },
                {
                    id: '22',
                    label: 'Nicollo Mackiavelly'
                },
                {
                    id: '23',
                    label: 'Rowen Atkinson'
                }
            ]
        }
    }
});