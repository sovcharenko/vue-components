Vue.component('extSelect', {

    template: `
        <span class="ext-select-container">
            <span class="ext-select" v-bind:class="{ 'expanded': containerExpanded }">
               <span class="ext-select-input-container">
                    <input class="ext-select-input" type="text" 
                        v-bind:placeholder="elPlaceholder" 
                        v-bind:value="current.label" 
                        v-model="currentInput" 
                        
                        @focus="expand"
                        @blur="close"
                        
                        @keyup="filter"
                        @keyup.down="move('down')"
                        @keyup.up="move('up')"
                        @keyup.enter="checkUpdateModel()">
                    <span v-if="current.id" class="ext-select-clear" @click="clear">&times;</span>
                    <span class="ext-select-arrow" @click="toggle">&#x25BE;</span>
               </span>
               <input type="hidden" v-bind:id="elId" v-bind:name="elName" v-bind:value="current.id">
               <ul class="el-container" v-bind:class="{ 'el-container-expanded': containerExpanded }">
                  <li v-for="el in filtered"
                    v-bind:data-value="el.id"
                    v-bind:class="{ 'el': true, 'el-selected':(el.id == current.id), 'el-not-found':(el.notFound) }"
                    
                    @mouseover="updateModel(el)"
                    @click="close">
                    {{el.label}}
                  </li>
               </ul>
            </span>
        </span>
    `,

    props: [
        'elId',
        'elName',
        'elValue',
        'elPlaceholder',
        'elData',
        'allowTags'
    ],

    data: function() {
        return {
            filtered: this.elData,
            currentInput: '',
            current: {
                id: (this.elValue ? this.elValue : ''),
                label: ''
            },

            containerExpanded: false
        };
    },

    methods: {
        filter: function() {
            if (['ArrowUp','ArrowDown','Enter'].indexOf(event.code) == -1) {
                if (!this.containerExpanded) this.expand();
                this.current = {};
                var filteredData = [];
                for(var i = 0; i < this.elData.length; i++) {
                    if ( this.elData[i].label.toLowerCase().indexOf(this.currentInput.toLowerCase()) != -1) {
                        filteredData.push(this.elData[i]);
                    }
                }
                if (filteredData.length < 1) {
                    filteredData.push({
                        id:'',
                        label: '"' + this.currentInput + '" not found',
                        notFound: true
                    });
                }
                this.filtered = filteredData;

            }
        },
        move: function (where) {
            for(var i = 0; i < this.filtered.length; i++) {
                if ( this.filtered[i].id == this.current.id) {
                    var elNum = (where == 'down' ? (i + 1) : (i - 1));
                    if (typeof this.filtered[elNum] === 'undefined') return;
                    this.updateModel( this.filtered[elNum] );
                    return;
                }
            }
            this.updateModel( this.filtered[0] );
        },

        clear: function () {
            this.filtered = this.elData;
            this.current = {};
            this.currentInput = '';
        },

        checkUpdateModel: function () {
            for(var i = 0; i < this.filtered.length; i++) {
                if ( this.filtered[i].id == this.current.id ) {
                    this.close();
                    return;
                }
            }
            if (this.allowTags) {
                this.current = {
                    id: this.currentInput,
                    label: this.currentInput
                };
            } else {
                this.updateInput('');
            }
            this.close();
        },

        updateModel: function (obj) {
            if (obj.hasOwnProperty('notFound')) return;
            this.current = obj;
            this.updateInput(this.current.label);
        },
        updateInput: function (val) {
            this.currentInput = val;
        },

        expand: function () {
            this.containerExpanded = true;
        },
        close: function () {
            this.containerExpanded = false;
        },
        toggle: function () {
            this.containerExpanded = !this.containerExpanded;
        }
    }
});