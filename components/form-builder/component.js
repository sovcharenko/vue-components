Vue.component('formBuilder', {

    template: ` 
        <div class="builder">
            
            <!-- Form -->
            <div class="form-container">
                <form :action="formAction">
                    <h1>{{ formName }}</h1>
                    <div v-for="field in fields" class="form-option">
                        
                        <div>
                            <label :for="field.id">{{ field.label }}</label>
                        </div>
                        
                        <div v-if="field.type.id == 'text'">
                            <input type="text" :id="field.id" :name="field.name" :placeholder="field.placeholder" :value="field.value">
                        </div>
                        
                        <div v-if="field.type.id == 'checkbox'">
                            <input type="checkbox" :id="field.id" :name="field.name" :placeholder="field.placeholder" :value="field.value">                        
                        </div>
                        
                        <div v-if="field.type.id == 'checkboxList'">
                            <div v-for="(option, index) in field.options">
                                <input type="checkbox" 
                                    :id="field.id + '-' + index" 
                                    :name="field.name + '[' + index + ']'" 
                                    :value="option.value"> {{ option.label }}
                            </div>
                        </div>
                        
                        <div v-if="field.type.id == 'radio'">
                            <div v-for="(option, index) in field.options">
                                <input type="radio" 
                                    :id="field.id + '-' + index" 
                                    :name="field.name" 
                                    :value="option.value"> {{ option.label }}
                            </div>
                        </div>
                        
                        <div v-if="field.type.id == 'select'">
                            <select :name="field.name" :id="field.id">
                                <option v-for="(option, index) in field.options" 
                                    :value="option.value">
                                    {{ option.label }}
                                </option>
                            </select>
                        </div>
                        
                    </div>
                </form>
            </div>
            
            <!-- Form Editor -->
            <div class="editor-container">
                <!-- Buttons  -->
                <a class="form-button" @click.prevent="openModal('formOptions')"><b>&#x2264;</b>  Form Options</a>
                <a class="form-button" @click.prevent="openModal('newField')"><b>+</b>  New Field</a>
                
                <!-- Form Options  -->
                <div class="editor-modal" :class="{'open': modal.formOptions}">
                    <div class="editor-modal-container">
                        <div class="editor-modal-header">
                            <h3>Form Options</h3>
                            <a class="editor-modal-close" @click.prevent="closeModal('formOptions')" href="#">&times;</a>
                        </div>
                        <div class="editor-modal-content">
                            <div class="form-options-container">
                                <div class="form-option">
                                    <div>
                                        <label for="formAction">Form Action</label>                
                                    </div>
                                    <div>
                                        <input v-model="formAction" id="formAction" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="formName">Form Name</label>            
                                    </div>
                                    <div>
                                        <input v-model="formName" id="formName" type="text">   
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for=""></label>
                                    </div>
                                    <div>
                                                    
                                    </div>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- New Field  -->
                <div class="editor-modal" :class="{'open': modal.newField}">
                    <div class="editor-modal-container">
                        <div class="editor-modal-header">
                            <h3>New Field</h3>
                            <a class="editor-modal-close" @click.prevent="closeModal('newField')" href="#">&times;</a>
                        </div>
                        <div class="editor-modal-content">
                            <div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldId">ID</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.id" id="newFieldId" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldName">Name</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.name" id="newFieldName" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldPlaceholder">Placeholder</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.placeholder" id="newFieldPlaceholder" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldLabel">Label</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.label" id="newFieldLabel" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldValue">Value</label>                
                                    </div>
                                    <div>
                                        <input v-model="newField.value" id="newFieldValue" type="text">            
                                    </div>                    
                                </div>
                                <div class="form-option">
                                    <div>
                                        <label for="newFieldType">Type:<span> {{ newField.type.label }}</span></label>                
                                    </div>
                                    <div>
                                        <select v-model="newField.type" id="newFieldType" type="text">
                                            <option v-for="type in fieldType" :value="type" >{{ type.label }}</option>
                                        </select>            
                                    </div>                    
                                </div>
                                <div v-if="newField.type.isOptional" class="form-option">
                                    <table border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="10%">#</td>
                                                <td width="40%">Label</td>
                                                <td width="40%">Value</td>
                                                <td width="10%">
                                                    <a class="form-button btn-i"
                                                            @click.prevent="newField.options.push(fieldOptionTPL)">
                                                            <b>+</b>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr v-for="(option, index) in newField.options">
                                                <td>{{ index + 1 }}</td>
                                                <td>
                                                    <input type="text">
                                                </td>
                                                <td>
                                                    <input type="text">
                                                </td>
                                                <td>
                                                    <a class="form-button btn-i"
                                                        v-if="(index + 1) > newField.type.minOptions"
                                                        @click.prevent="newField.options.splice(index, 1)">
                                                        &times;
                                                    </a>
                                                </td>
                                            </tr> 
                                        </tbody>
                                       
                                    </table>                    
                                </div>
                            </div>
                        </div>
                        <div class="editor-modal-footer text-right">
                            <a class="form-button"
                                @click="addField">
                                &#x2714;  Add field
                            </a>
                        </div>
                    </div>
                </div>
                
                <div>
                </div>
                
            </div>
        <div>
            
    `,

    props: [

    ],

    data: function() {
        return {

            // -- Form Options
            formAction: '/',
            formName: 'Form',
            formID: this.formName,
            formTitle: 'Form',
            wpapNamesArray: true,

            // -- Modals
            modal: {
                formOptions: false,
                newField: false,
                editField: false
            },

            // -- New Field
            fieldTPL: {
                id: '',
                name: '',
                type: '',
                label: '',
                placeholder: '',
                value: '',
                options: []
            },
            fieldOptionTPL: {
                value: '',
                label: ''
            },
            newField: {},
            currentField: {},
            fieldType: [
                {
                    id: 'text',
                    label: 'Text',
                    isOptional: false
                },
                {
                    id: 'select',
                    label: 'Drop-Down List',
                    isOptional: true,
                    minOptions: 2
                },
                {
                    id: 'checkbox',
                    label: 'Checkbox',
                    isOptional: false
                },
                {
                    id: 'radio',
                    label: 'Radio List',
                    isOptional: true,
                    minOptions: 2
                },
                {
                    id: 'checkboxList',
                    label: 'Checkbox List',
                    isOptional: true,
                    minOptions: 2
                }
            ],

            // -- Fields options
            fields: []
            // wpapNamesArray: true

        };
    },

    created: function() {
        // -- Init
        this.newField = this.fieldTPL;
        this.currentField = this.fieldTPL;

        // -- Default options of the conditional field
        this.$watch('newField.type', function (val, oldVal) {
            if (typeof val != 'undefined' && val.isOptional) {
                var opts = [];
                for(var i = 0; i < val.minOptions; i++)
                    opts.push(this.fieldOptionTPL);
                this.newField.options = opts;
            } else this.newField.options = [];
        })
    },

    methods: {

        // -- Modals
        openModal: function (modalName) {
            for(var m in this.modal) this.modal[m] = false;
            this.modal[modalName] = true;
        },
        closeModal: function (modalName) {
            this.modal[modalName] = false;
        },

        // -- Form fields
        addField: function () {
            console.log(this)
            this.fields.push( JSON.parse( JSON.stringify( this.newField ) ) );
            this.newField = {};
            this.closeModal('newField');
        },

        // -- Field Types
        // getTypeLabel: function (type) {
        //     var typeObject = this.findBy(this.fieldType, 'id', type);
        //     if (typeObject) return ': ' + typeObject.label;
        //     return '';
        // },
        // isOptionalType: function (type) {
        //     return this.findBy(this.fieldType, 'id', type).isOptional;
        // },

        // -- Helpers
        findBy: function (array, key, value) {
            if (Array.isArray(array)) {
                for(var i = 0; i < array.length; i++)
                    if (array[i][key] == value) return array[i];
            } else {
                for (var i in array)
                    if (array[i].hasOwnProperty(key) && (array[i][key] == value)) return array[i];
            }
            return false;
        },
        filterBy: function (array, key, value) {
            if (Array.isArray(array)) {
                return array.filter(function (element) {
                    return (element[key] == value);
                });
            } else {
                var result = {}, element;
                for (element in array) {
                    if (array[element].hasOwnProperty(key) && (array[element][key] == value)) {
                        result[element] = array[element];
                    }
                }
                return result;
            }
        }
    }
});