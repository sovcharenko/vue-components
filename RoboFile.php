<?php

include('robo-config/AssetsConfig.php');
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    use AssetsConfig;

    /**
     * Combines and minifies static
     */
    public function assetsGo()
    {
        foreach (self::concat() as $target => $files)
            $this->taskConcat($files)->to($target)->run();
        foreach (self::minify() as $target => $files)
            $this->taskMinify($files)->to($target)->run();
    }

    /**
     * Watches for a changes in a given resources
     */
    public function assetsWatch()
    {
        $watcher = $this->taskWatch();
        foreach (self::watch() as $target => $action) {
            $watcher->monitor(
                $target,
                function() use($action) {
                    $this->$action();
                }
            );
        }
        $watcher->run();
    }

}