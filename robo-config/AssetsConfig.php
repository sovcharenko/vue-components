<?php

/**
 * Created by PhpStorm.
 * User: Sebastian
 * Date: 04.03.2017
 * Time: 17:14
 */
trait AssetsConfig
{
    static $ASSETS = 'assets/';
    static $COMP = 'components/';
    static $JS_COMPONENT = '/component.js';
    static $JS_CONTAINER = '/container.js';
    static $CSS_FILE = '/style.css';

    static function concat() {
        return [
            self::$ASSETS . 'style.css' => [
                self::$COMP . 'extselect' . self::$CSS_FILE,

                self::$COMP . 'form-builder' . self::$CSS_FILE,

                self::$ASSETS . 'layout/component.css'
            ],

            self::$ASSETS . 'script.js' => [
                self::$COMP . 'extselect' . self::$JS_COMPONENT,
                self::$COMP . 'extselect' . self::$JS_CONTAINER,

                self::$COMP . 'form-builder' . self::$JS_COMPONENT,
                self::$COMP . 'form-builder' . self::$JS_CONTAINER,

                self::$ASSETS . 'layout/component.js'
            ],
        ];
    }

    static function minify() {
        return [
            self::$ASSETS . 'style.min.css' => self::$ASSETS . 'style.css',
        ];
    }

    static function watch() {
        return [
            self::$COMP => 'assetsGo',
            self::$ASSETS . 'layout' => 'assetsGo',
        ];
    }

}